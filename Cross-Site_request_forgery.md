What is Cross-Site Request Forgery (CSRF)?

This type of attack, also known as CSRF or XSRF, Cross-Site Reference Forgery, Hostile Linking, and more, allow an attacker to carry out actions (requests) within an application where a user is currently logged in. It is “cross-site” or “cross-origin” because it uses different websites or elements to interfere, i.e., to send requests within an application that originate from outside of the application.

A CSRF sends an HTTP request whenever a user opens a website containing malicious code to achieve its aim. The code is embedded in such a way that no further actions by the user are required. 

This kind of attack is widely used in spam emails. By clicking on a malicious URL, the attack starts without the user’s knowledge and forges actions. The HTTP requests sent without the user’s knowledge may access, modify or delete sensitive data.

While CSRF attacks mainly occur through users’ browsers, they can effectively be executed through any file that allows scripting, including word and XML documents, RSS feeds, and more. 

In this article you’ll find everything about these attacks, but… if you don’t have enough time to read, check our CSRF Scanner to discover Cross-Site Request Forgery vulnerabilities you could be exposed to (it takes just 2 minutes to scan your web app and it’s for free!).




How does a CSRF attack work?

CSRF works by an attacker gaining access to a victim’s browser – typically through a malicious link. That access is then used to make a malicious request to any application with a currently active session in which the user is authenticated. 

Because the attacker has access to the browser, requests sent from it are seen by applications as legitimate because they are sent along with the session cookie that allows the request to occur. 

For a CSRF to have an effect and be meaningful, several conditions must be present:

There must be an action that the attacker wants to execute on behalf of the victim that will provide the attacker with some benefit (i.e., gain access to and change a victim’s password)
The application must use session cookies to authenticate the request made by the user
The attacker knows the parameters of the requests he wants to perform





GET Requests

Some applications use HTTP GET requests to perform state changes, such as changing a password. In this case, an attacker will send a malicious link (that mimics an ordinary one). When a user visits the link, a script that triggers the browser to perform the request is executed. 

Alternatively, the attack may be executed by sending victims to a page that loads the address through an HTML element, such as an image.

In either instance, the browser’s GET request will be executed in the background to achieve the desired result without indicating to the user that this has occurred. Thus, CSRF GET request exploits are relatively trivial but still appear as vulnerabilities in different applications.


POST Requests

The main difference between using a GET and POST request for a CSRF is how the request is submitted. The reason attackers use HTTP POST is that state-changing requests in applications are frequently made in this way. 

Unlike GET, where the attack is achieved through a URL containing the request, the victim’s browser sends the desired values through the request body with a POST request. These are typically encoded in the form of a query string.

An attacker may use the <form> element to submit this request, which will require the user to submit the request manually. This can be achieved by tricking the victim into clicking on a button on the malicious website to which they are led. Alternatively, the attacker may embed JavaScript in the website, which will execute the form request automatically.

The same result can also be achieved by using an iFrame embedded as a hidden field by the attacker. Much like with JavaScript, the iFrame can be created to send the request as soon as it is loaded on the website.



Other Requests


Another way a CSRF can be successful is by using different HTTP elements such as PUT or DELETE. These can be submitted as part of a JSON or XML string but are prevented by modern browsers by default because of the same-origin policy (SOP) restrictions and cross-origin resource sharing (CORS). For such a request to be allowed, these restrictions must have been manually removed on a website, allowing it to receive requests with different origins. 

A CSRF can also be executed as part of a cross-site scripting (XSS) attack. In this case, the CSRF will be part of the payload attached to the XSS, like with the famous Samy worm used in MySpace.




How to Prevent Cross-Site Request Forgery Attacks
To prevent CSRF injection attacks, you must make sure that an attacker cannot craft an arbitrary request run in the security context of any other user and send from a different website. This is one of the main conditions that need to be in place for a CSRF attack to be successful. Disrupting this condition prevents the possibility of such an attack.

Token-based prevention
As stated by the OWASP Cross-Site Request Forgery Prevention Cheat Sheet, the most common mitigation technique for cross-site request forgery attacks is using a CSRF token (also known as a synchronizer token or anti-CSRF token). These session tokens are unpredictable and unique values generated by the application and sent to the client. After that, they are sent back in the request made by the client to the server, which verifies the request. 

This introduces an unknown element that can effectively defuse the CSRF attack. Any request that does not originate from the original form will not include the correct value for the CSRF token and can be easily discarded. 

Common CSRF token vulnerabilities
Yet, some vulnerabilities can arise in the use of CSRF tokens due to omissions in the procedure. The most common CSRF token vulnerabilities include:

Tokens are validated and used only when POST requests are made and not when GET requests are made
Validation occurs only if the session token is present, and if it is omitted, validation is also skipped
Tokens are not tied to the current user session but are compared to tokens issued at any point by the application
Tokens are tied to a cookie but not one that is used to track the current session
Applications do not maintain a record of tokens – instead, the token is included in the cookie and the application verifies that the token in the request is the same as the one in the cookie
All of the above constitute vulnerabilities that can open the door to a CSRF attack. To defend against such an attack, CSRF tokens need to be implemented correctly, along with several other mitigation techniques.

When using modern frameworks, tokens-based CSRF protection is typically included or can easily be added to forms and validated by the corresponding middleware. Furthermore, for single-page applications, the CSRF token may be provided by a meta tag which is then read from the JavaScript in the browser and amended to every request. 

Double-submit cookie prevention
A double-submit cookie token approach can be used if using a valid token on the server-side is impossible. In this cookie-based session handling, when a user visits a website, the site generates a value that stores as a cookie on the user’s device, apart from the cookie that serves as a session identifier.

When a legitimate request is submitted to the site, it must contain the same value as included in the cookie. The server then verifies this, and if the values match, the request parameter is accepted.

Same-site cookie prevention
As recommended by OWASP, this approach should be used together with a CSRF token strategy and not as a substitute. 

The same-site cookie approach restricts the origin from which a cookie can be sent. Thus, CSRF exploits the possibility of making a cross-origin request (and hence same-site cookies). By limiting requests so that they can only be sent from the origin to which a cookie is related, the ability to send external requests to an application is effectively prevented.

Custom request header
A technique that is particularly effective for AJAX or API endpoints is the use of custom request headers. In this approach, JavaScript is used to add a custom header. Unfortunately, JavaScript can’t make cross-origin requests with a custom header because of the SOP security restrictions.

This prevents sending a cross-domain request with custom headers, thereby eliminating the possibility of a CSRF attack.