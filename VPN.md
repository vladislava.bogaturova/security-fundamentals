A virtual private network (VPN) extends a private network across a public network and enables users to send and receive data across shared or public networks as if their computing devices were directly connected to the private network.[1] The benefits of a VPN include increases in functionality, security, and management of the private network. It provides access to resources that are inaccessible on the public network and is typically used for remote workers. Encryption is common, although not an inherent part of a VPN connection

A VPN is created by establishing a virtual point-to-point connection through the use of dedicated circuits or with tunneling protocols over existing networks. A VPN available from the public Internet can provide some of the benefits of a wide area network (WAN). From a user perspective, the resources available within the private network can be accessed remotely

Types

VPN classification tree based on the topology first, then on the technology used.

VPN connectivity overview, showing intranet site-to-site and remote-work configurations used together
Virtual private networks may be classified by several categories:

Remote access
A host-to-network configuration is analogous to connecting a computer to a local area network. This type provides access to an enterprise network, such as an intranet. This may be employed for remote workers who need access to private resources, or to enable a mobile worker to access important tools without exposing them to the public Internet.
Site-to-site
A site-to-site configuration connects two networks. This configuration expands a network across geographically disparate offices, or a group of offices to a data center installation. The interconnecting link may run over a dissimilar intermediate network, such as two IPv6 networks connected over an IPv4 network.[4]
Extranet-based site-to-site
In the context of site-to-site configurations, the terms intranet and extranet are used to describe two different use cases.[5] An intranet site-to-site VPN describes a configuration where the sites connected by the VPN belong to the same organization, whereas an extranet site-to-site VPN joins sites belonging to multiple organizations.
Typically, individuals interact with remote access VPNs, whereas businesses tend to make use of site-to-site connections for business-to-business, cloud computing, and branch office scenarios. Despite this, these technologies are not mutually exclusive and, in a significantly complex business network, may be combined to enable remote access to resources located at any given site, such as an ordering system that resides in a datacenter.

VPN systems also may be classified by:

The tunneling protocol used to tunnel the traffic
The tunnel's termination point location, e.g., on the customer edge or network-provider edge
The type of topology of connections, such as site-to-site or network-to-network
The levels of security provided
The OSI layer they present to the connecting network, such as Layer 2 circuits or Layer 3 network connectivity
The number of simultaneous connections

Security mechanisms
VPNs cannot make online connections completely anonymous, but they can increase privacy and security. To prevent disclosure of private information or data sniffing, VPNs typically allow only authenticated remote access using tunneling protocols and secure encryption techniques.

The VPN security model provides:

Confidentiality such that even if the network traffic is sniffed at the packet level (see network sniffer or deep packet inspection), an attacker would see only encrypted data, not the raw data.
Sender authentication to prevent unauthorized users from accessing the VPN.
Message integrity to detect and reject any instances of tampering with transmitted messages.

The life cycle phases of an IPSec Tunnel in a virtual private network.
Secure VPN protocols include the following:

Internet Protocol Security (IPsec) was initially developed by the Internet Engineering Task Force (IETF) for IPv6, which was required in all standards-compliant implementations of IPv6 before RFC 6434 made it only a recommendation.[6] This standards-based security protocol is also widely used with IPv4 and the Layer 2 Tunneling Protocol. Its design meets most security goals: availability, integrity, and confidentiality. IPsec uses encryption, encapsulating an IP packet inside an IPsec packet. De-encapsulation happens at the end of the tunnel, where the original IP packet is decrypted and forwarded to its intended destination.
Transport Layer Security (SSL/TLS) can tunnel an entire network's traffic (as it does in the OpenVPN project and SoftEther VPN project[7]) or secure an individual connection. A number of vendors provide remote-access VPN capabilities through SSL. An SSL VPN can connect from locations where IPsec runs into trouble with Network Address Translation and firewall rules.
Datagram Transport Layer Security (DTLS) – used in Cisco AnyConnect VPN and in OpenConnect VPN[8] to solve the issues TLS has with tunneling over TCP (SSL/TLS are TCP-based, and tunneling TCP over TCP can lead to big delays and connection aborts[9]).
Microsoft Point-to-Point Encryption (MPPE) works with the Point-to-Point Tunneling Protocol and in several compatible implementations on other platforms.
Microsoft Secure Socket Tunneling Protocol (SSTP) tunnels Point-to-Point Protocol (PPP) or Layer 2 Tunneling Protocol traffic through an SSL/TLS channel (SSTP was introduced in Windows Server 2008 and in Windows Vista Service Pack 1).
Multi Path Virtual Private Network (MPVPN). Ragula Systems Development Company owns the registered trademark "MPVPN".[10]
Secure Shell (SSH) VPN – OpenSSH offers VPN tunneling (distinct from port forwarding) to secure remote connections to a network, inter-network links, and remote systems. OpenSSH server provides a limited number of concurrent tunnels. The VPN feature itself does not support personal authentication.[11][12][13] SSH is more often used to remotely connect to machines or networks instead of a site to site VPN connection.
WireGuard is a protocol. In 2020, WireGuard support was added to both the Linux[14] and Android[15] kernels, opening it up to adoption by VPN providers. By default, WireGuard utilizes the Curve25519 protocol for key exchange and ChaCha20-Poly1305 for encryption and message authentication, but also includes the ability to pre-share a symmetric key between the client and server.[16][17]
IKEv2 is an acronym that stands for Internet Key Exchange version 2. It was created by Microsoft and Cisco and is used in conjunction with IPSec for encryption and authentication. Its primary use is in mobile devices, whether on 3G or 4G LTE networks, since it is automatically reconnecting when a connection is lost.
OpenVPN is a free and open-source VPN protocol that is based upon the TLS protocol. It supports perfect forward-secrecy, and most modern secure cipher suits, like AES, Serpent, TwoFish, etc. It is currently being developed and updated by OpenVPN Inc., a non-profit providing secure VPN technologies