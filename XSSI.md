Basic Information
XSSI designates a kind of vulnerability which exploits the fact that, when a resource is included using the script tag, the SOP doesn’t apply, because scripts have to be able to be included cross-domain. An attacker can thus read everything that was included using the script tag.
This is especially interesting when it comes to dynamic JavaScript or JSONP when so-called ambient-authority information like cookies are used for authentication. The cookies are included when requesting a resource from a different host.
Types
Static JavaScript (regular XSSI)
Static JavaScript, which is only accessible when authenticated
Dynamic JavaScript
Non-JavaScript


Regular XSSI
The private information is located inside a global accessible JS file, you can just detect this by reading files, searching keywords or using regexps.
To exploit this, just include the script with private information inside the malicious content:

<script src="https://www.vulnerable-domain.tld/script.js"></script>
<script> alert(JSON.stringify(confidential_keys[0])); </script>

Non-Script-XSSI
Takeshi Terada describes another kind of XSSI in his paper Identifier based XSSI attacks. He was able to leak Non-Script files cross-origin by including, among others, CSV files as source in the script tag, using the data as variable and function names.
The first publicly documented XSSI attack was in 2006. Jeremiah Grossman’s blog entry Advanced Web Attack Techniques using GMail depicts a XSSI, which by overriding the Array constructor was able to read the complete address book of a google account.
In 2007 Joe Walker published JSON is not as safe as people think it is. He uses the same idea to steal JSON that is inside an Array.
Other related attacks were conducted by injecting UTF-7 encoded content into the JSON to escape the JSON format. It is described by Gareth Heyes, author of Hackvertor, in the blog entry JSON Hijacking released in 2011. In a quick test, this was still possible in Microsoft Internet Explorer and Edge, but not in Mozilla Firefox or Google Chrome.
JSON with UTF-7:
[{'friend':'luke','email':'+ACcAfQBdADsAYQBsAGUAcgB0ACgAJwBNAGEAeQAgAHQAaABlACAAZgBvAHIAYwBlACAAYgBlACAAdwBpAHQAaAAgAHkAbwB1ACcAKQA7AFsAewAnAGoAbwBiACcAOgAnAGQAbwBuAGU-'}]
Including the JSON in the attacker’s page
<script src="http://site.tld/json-utf7.json" type="text/javascript" charset="UTF-7"></script>