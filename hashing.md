What is hashing?
Hashing is the process of transforming any given key or a string of characters into another value. This is usually represented by a shorter, fixed-length value or key that represents and makes it easier to find or employ the original string.

The most popular use for hashing is the implementation of hash tables. A hash table stores key and value pairs in a list that is accessible through its index. Because key and value pairs are unlimited, the hash function will map the keys to the table size. A hash value then becomes the index for a specific element.

A hash function generates new values according to a mathematical hashing algorithm, known as a hash value or simply a hash. To prevent the conversion of hash back into the original key, a good hash always uses a one-way hashing algorithm.

Hashing is relevant to -- but not limited to -- data indexing and retrieval, digital signatures, cybersecurity and cryptography. 


What is hashing used for?
Data retrieval
Hashing uses functions or algorithms to map object data to a representative integer value. A hash can then be used to narrow down searches when locating these items on that object data map.

For example, in hash tables, developers store data -- perhaps a customer record -- in the form of key and value pairs. The key helps identify the data and operates as an input to the hashing function, while the hash code or the integer is then mapped to a fixed size.

Hash tables support functions that include the following:

insert (key, value)
get (key)
delete (key)


What is hashing in cryptography?
cryptography uses multiple hash functions to secure data. Some of the most popular cryptographic hashes include the following:

Secure Hash Algorithm 1 (SHA-1)
Secure Hash Algorithm 2 (SHA-2)
Secure Hash Algorithm 3 (SHA-3)
MD2
MD4
MD5


Message-digest hash functions like MD2, MD4 and MD5 help hash digital signatures. Once hashed, the signature is transformed into a shorter value called a message digest.

Secure Hash Algorithm (SHA) is a standard algorithm used to create a larger (160-bit) message digest. While it is like message-digest hash function MD4 -- and is good at database storage and retrieval -- this is not the best approach for cryptographic or error-checking purposes. SHA-2 is used to create a larger (224-bit) message digest. SHA-3 is SHA-2's successor.